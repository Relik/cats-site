import React from "react";
import { mount, configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import { TextField } from "@material-ui/core";
import InputText from "./InputText";
import { act } from "react-dom/test-utils";
configure({ adapter: new Adapter() });

const mockOnChange = jest.fn(value => {
  return value;
});
const mockError = "Mock error";
const mockLabel = "Mock label";
const wrapper = mount(
  <InputText onChange={mockOnChange} error={mockError} label={mockLabel} />
);

it("Highlites error on first blur", () => {

  let textField = wrapper.find(TextField).first();

  expect(textField.prop("error")).toBe(false);
  expect(textField.prop("helperText")).toBe("");
  act(wrapper.find(TextField).first().prop("onBlur"));
  expect(mockOnChange).toHaveBeenCalledTimes(1);
  wrapper.update();
  expect(wrapper.find(TextField).first().prop("helperText")).toBe(mockError);
  expect(wrapper.find(TextField).first().prop("error")).toBe(true);

});

import React, { useEffect, useState } from "react";
import TextField from "@material-ui/core/TextField";

export default function InputText(props) {
  const [hideError, setHideError] = useState(true);

  useEffect(() => {
    if (!props.hideError) setHideError(false);
    return () => {};
  }, [hideError, props.hideError]);

  return (
    <TextField
      error={!hideError && props.error !== ""}
      helperText={hideError ? "" : props.error}
      id="filled-basic"
      label={props.label}
      value={props.value}
      variant="filled"
      onChange={e => {
        props.onChange(e);
      }}
      onBlur={e => {
        if (hideError) {
          setHideError(false);
          console.log(e);
          props.onChange(e);
        }
      }}
    />
  );
}
InputText.defaultProps = {
  hideError: true
};

import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import { Button } from "@material-ui/core";

export default function Header(props) {
  return (
    <div>
      <AppBar position="static">
        <Toolbar>
          <Button
            onClick={e => {
              e.preventDefault();
              props.history.push("/");
            }}
            variant="contained"
            color="primary"
          >
            <Typography variant="h6">Cat Site</Typography>
          </Button>
        </Toolbar>
      </AppBar>
    </div>
  );
}

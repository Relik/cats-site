import React from "react";
import { mount, configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import DropMenu from "./DropMenu";
import { FormControl } from "@material-ui/core";
configure({ adapter: new Adapter() });

const mockOptions = [ '1', '2', '3'];

const wrapper = mount(
    <DropMenu />
  );

it("Renders without any options props", () => {
    expect(wrapper.find(FormControl).exists()).toBe(true);
    expect(wrapper.prop('options')).toEqual([]);
});

it("Renders menu items when given array of options", () => {
    wrapper.setProps({options: mockOptions});
    expect(wrapper.prop('options')).toEqual(mockOptions);
});


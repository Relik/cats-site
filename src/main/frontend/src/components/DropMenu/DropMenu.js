import React from "react";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 160
  },
  selectEmpty: {
    marginTop: theme.spacing(2)
  }
}));

export default function DropMenu(props) {
  const classes = useStyles();
  
  return (
    <FormControl variant="filled" className={classes.formControl}>
      <InputLabel id={"dropmenu-select"}>{props.label}</InputLabel>
      <Select
        labelId={"dropmenu-select"}
        label={props.label}
        value={props.value}
        onChange={props.onChange}
      >
        {props.options.map((type, index) => {
          return (
            <MenuItem value={type} key={index} selected={index === 1}>
              {type}
            </MenuItem>
          );
        })}
      </Select>
    </FormControl>
  );
}
DropMenu.defaultProps = {
  options: [],
  value: ''
};

import React from "react";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import { useUserContext } from "../../contexts/UserContext";
import { clearAlert } from "../../actions/User/User";

export default function AlertManager() {
  const { user, dispatch } = useUserContext();

  const handleClose = (e) => {
    clearAlert(dispatch);
  };
  return(
    <div>
    <Snackbar
      open={user.alert.msg !== ""}
      autoHideDuration={6000}
      onClose={handleClose}
    >
      <MuiAlert
        elevation={6}
        variant="filled"
        onClose={handleClose}
        severity={user.alert.type}
      >
        {user.alert.msg}
      </MuiAlert>
    </Snackbar>
    </div>
  );
}

import React, { useReducer } from "react";
import { mount, configure } from "enzyme";
import AlertManager from "./AlertManager";
import {
  UserContext,
  userReducer,
  useUserContext
} from "../../contexts/UserContext";
import { alertTypes, clearAlert } from "../../actions/User/User";
import Adapter from "enzyme-adapter-react-16";
import { Snackbar } from "@material-ui/core";
import Alert from "@material-ui/lab/Alert";

jest.mock("../../contexts/UserContext");
jest.mock("../../actions/User/User");

configure({ adapter: new Adapter() });
const mockUserState = {
  alert: {
    msg: "Test alert",
    type: alertTypes.warning
  },
  users: []
};

const MockUserProvider = props => {
  const [user, dispatch] = useReducer(userReducer, mockUserState);

  return <UserContext.Provider value={{ user, dispatch }} {...props} />;
};

const mockDispatch = jest.fn();
useUserContext.mockReturnValue({
  user: mockUserState,
  dispatch: mockDispatch
});

clearAlert.mockReturnValue();

const wrapper = mount(
  <MockUserProvider>
    <AlertManager />
  </MockUserProvider>
);
it("Properly opens when got alert message", () => {

  const snackbar = wrapper.find(Snackbar).first();
  const alert = wrapper.find(Alert).first();

  expect(snackbar.prop("open")).toBe(true);
  expect(alert.exists()).toBe(true);
  expect(alert.text()).toBe(mockUserState.alert.msg);
});
it("Properly calls 'clearAlert' when close event is called", () => {

  const snackbar = wrapper.find(Snackbar).first();
  const alert = wrapper.find(Alert).first();

  snackbar.prop("onClose")();
  alert.prop("onClose")();
  
  expect(clearAlert).toHaveBeenCalledTimes(2);
  wrapper.update();
});

it("Is closed when doesn't have a alert message", () => {
  mockUserState.alert.msg = "";
  const wrapper = mount(
    <MockUserProvider>
      <AlertManager />
    </MockUserProvider>
  );

  const snackbar = wrapper.find(Snackbar).first();
  const alert = wrapper.find(Alert).first();
  expect(snackbar.prop("open")).toBe(false);
  expect(alert.exists()).toBe(false);
});

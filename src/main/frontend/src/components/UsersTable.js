import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import Paper from "@material-ui/core/Paper";

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(el => el[0]);
}
const headCells = [
  { id: "forename", numeric: false, disablePadding: false, label: "Forename" },
  { id: "surname", numeric: false, disablePadding: false, label: "Surname" },
  { id: "email", numeric: false, disablePadding: false, label: "Email" },
  {
    id: "isCatLover",
    numeric: false,
    disablePadding: false,
    label: "Loves cats"
  },
  {
    id: "favoriteBreed",
    numeric: false,
    disablePadding: false,
    label: "Favorite Breed"
  },
  {
    id: "reasonForDislike",
    numeric: false,
    disablePadding: false,
    label: "Why dislikes cats?"
  },
  {
    id: "emailConfirmation",
    numeric: false,
    disablePadding: false,
    label: "Email Confirmation"
  }
];

function EnhancedTableHead(props) {
  const { classes, order, orderBy, onRequestSort } = props;
  const createSortHandler = property => event => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map(headCell => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? "right" : "left"}
            padding={headCell.disablePadding ? "none" : "default"}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : "asc"}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === "desc" ? "sorted descending" : "sorted ascending"}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  order: PropTypes.oneOf(["asc", "desc"]).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired
};

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%"
  },
  paper: {
    width: "100%",
    marginBottom: theme.spacing(2)
  },
  table: {
    minWidth: 750
  },
  visuallyHidden: {
    border: 0,
    clip: "rect(0 0 0 0)",
    height: 1,
    margin: -1,
    overflow: "hidden",
    padding: 0,
    position: "absolute",
    top: 20,
    width: 1
  },
  catLover: {
    color: "limegreen"
  }
}));

export default function UsersTable(props) {
  const classes = useStyles();
  const [order, setOrder] = React.useState("asc");
  const [orderBy, setOrderBy] = React.useState("surname");
  const [selectedId, setSelectedId] = React.useState(-1);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [rows, setRows] = React.useState([]);

  useEffect(() => {
    setRows(props.users);
    return () => {};
  }, [props.users, rows]);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const handleClick = user => {
    if (user.id === selectedId) props.onRowDoubleClick(user);
    setSelectedId(user.id);
    setTimeout(() => {
      setSelectedId(-1);
    }, 1000);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const emptyRows =
    rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <TableContainer>
          <Table
            className={classes.table}
            aria-labelledby="Users"
            size={"medium"}
            aria-label="Users table"
          >
            <EnhancedTableHead
              classes={classes}
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
              rowCount={rows.length}
            />
            <TableBody>
              {stableSort(rows, getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {
                  return (
                    <TableRow
                      hover
                      onClick={() => handleClick(row)}
                      tabIndex={-1}
                      key={row.id}
                      className={row.isCatLover ? classes.catLover : ""}
                    >
                      <TableCell
                        className={row.isCatLover ? classes.catLover : ""}
                        align="left"
                      >
                        {row.forename}
                      </TableCell>
                      <TableCell
                        className={row.isCatLover ? classes.catLover : ""}
                        align="left"
                      >
                        {row.surname}
                      </TableCell>
                      <TableCell
                        className={row.isCatLover ? classes.catLover : ""}
                        align="left"
                      >
                        {row.email}
                      </TableCell>
                      <TableCell
                        align="left"
                        className={row.isCatLover ? classes.catLover : ""}
                      >
                        {row.isCatLover ? "Yes" : "No"}
                      </TableCell>
                      <TableCell
                        align="left"
                        className={row.isCatLover ? classes.catLover : ""}
                      >
                        {row.favoriteBreed || "X"}
                      </TableCell>
                      <TableCell
                        align="left"
                        className={row.isCatLover ? classes.catLover : ""}
                      >
                        {row.reasonForDislike || "X"}
                      </TableCell>
                      <TableCell
                        align="left"
                        className={row.isCatLover ? classes.catLover : ""}
                      >
                        {row.emailConfirmation ? "Yes" : "No"}
                      </TableCell>
                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow style={{ height: 53 * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
    </div>
  );
}

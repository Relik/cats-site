import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';

export default function ConfirmationDialog(props) {

  return (
      <Dialog
        open={props.open}
        onClose={()=>props.onClose(false)}
      >
        <DialogContent>
          <DialogContentText>
            {props.message}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={()=>props.onClose(false)} color="primary" autoFocus>
            No
          </Button>
          <Button onClick={()=>props.onClose(true)} color="secondary">
            Yes
          </Button>
        </DialogActions>
      </Dialog>
  );
}
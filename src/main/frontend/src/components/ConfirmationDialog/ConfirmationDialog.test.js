import React from "react";
import { mount, configure } from "enzyme";
import ConfirmationDialog from "./ConfirmationDialog";
import Adapter from "enzyme-adapter-react-16";
import { DialogContentText, Dialog, Button, DialogContent, DialogActions } from "@material-ui/core";

configure({ adapter: new Adapter() });

const mockOnClose = jest.fn(value => {return value});

it("Properly opens and closes", () => {
  const mockMessage = "Test message";
  const wrapper = mount(
      <ConfirmationDialog onClose={mockOnClose} message={mockMessage} open={true}/>
  );

  const dialogText = wrapper.find(DialogContentText).first();
  const dialog = wrapper.find(Dialog).first();
  const noButton = wrapper.find(Button).first();
  const yesButton = wrapper.find(Button).at(1);

  expect(dialog.exists()).toBe(true);
  expect(dialogText.exists()).toBe(true);
  expect(noButton.exists()).toBe(true);
  expect(yesButton.exists()).toBe(true);
  
  expect(dialogText.text()).toBe(mockMessage);
  noButton.prop('onClick')();
  yesButton.prop('onClick')();
  expect(mockOnClose).toHaveBeenCalledTimes(2);

  wrapper.setProps({open: false});
  wrapper.update();
  expect(wrapper.prop('open')).toBe(false); 
  expect(wrapper.find(DialogContent).first().exists()).toBe(true);
  expect(wrapper.find(DialogActions).first().exists()).toBe(true);
});

import React, { createContext, useReducer, useContext } from "react";
import * as ActionsTypes from "../actions/User/UserActionsTypes";
import { alertTemplate } from "../actions/User/User";

const initialState = {
  alert: alertTemplate,
  users: []
};
export const UserContext = createContext(initialState);

export function userReducer(state, action) {
  switch (action.type) {
    case ActionsTypes.HANDLED_ALERT:
      return { ...state, alert: { msg: "", type: state.alert.type } };
    case ActionsTypes.RAISED_ALERT:
      return { ...state, alert: action.alert };
    case ActionsTypes.GOT_USERS:
      return { ...state, users: action.users };
    default:
      return state;
  }
}

function UserProvider(props) {
  const [user, dispatch] = useReducer(userReducer, initialState);

  return <UserContext.Provider value={{ user, dispatch }} {...props} />;
}

function useUserContext() {
  return useContext(UserContext);
}

export { UserProvider, useUserContext };

import * as ActionsTypes from './UserActionsTypes';

export function gotUsers(users) {
  return {
    type: ActionsTypes.GOT_USERS,
    users
  };
}

export function error(err) {
  return {
    type: ActionsTypes.ERROR,
    err
  };
}
export function raisedAlert(alert) {
  return {
    type: ActionsTypes.RAISED_ALERT,
    alert
  };
}

export function handledAlert() {
  return {
    type: ActionsTypes.HANDLED_ALERT
  };
}
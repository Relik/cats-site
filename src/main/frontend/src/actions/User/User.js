import axios from "axios";
import * as actions from "./UserActions";
import SockJS from "sockjs-client";
import Stomp from "stompjs";

export const userTemplate = {
  forename: "",
  surname: "",
  email: "",
  isCatLover: true,
  favoriteBreed: "None",
  reasonForDislike: "",
  emailConfirmation: false
};

export const alertTypes = {
  success: "success",
  info: "info",
  warning: "warning",
  error: "error"
};

export const alertTemplate = {
  type: alertTypes.success,
  msg: ""
};

export const registerUser = (user, dispatch) => {
  if (user.isCatLover) user.reasonForDislike = "";
  else user.favoriteBreed = "";

  return axios
    .post(`${process.env.REACT_APP_HOST}/user/register`, user)
    .then(res => {
      dispatch(actions.gotUsers(res.data));
      dispatch(
        actions.raisedAlert({
          type: alertTypes.success,
          msg: `User: ${user.forename} ${user.surname} registered`
        })
      );
      return res.data;
    })
    .catch(err => {
      dispatch(
        actions.raisedAlert({
          type: alertTypes.error,
          msg: err.response ?  err.response.data : err.toString()
        })
      );
      dispatch(actions.error(err));
      throw err;
    });
};

export const getUsers = dispatch => {
  return axios
    .get(`${process.env.REACT_APP_HOST}/user/users`)
    .then(res => {
      dispatch(actions.gotUsers(res.data));
      return res.data;
    })
    .catch(err => {
      dispatch(actions.error(err));
      throw err;
    });
};

export const subscribeUsers = dispatch => {
  const socket = new SockJS(`${process.env.REACT_APP_HOST}/socket`);
  const stompClient = Stomp.over(socket);
  stompClient.debug = function(str) {};
  stompClient.connect({}, () => {
    stompClient.subscribe(
      "/new",
      res => {
        dispatch(actions.gotUsers(JSON.parse(res.body)));
        return stompClient;
      },
      err => {
        console.log("Subscribe error: ", err);
        return;
      }
    )
  }, err => {
    console.log("Socket 2 error: ", err);
    stompClient.disconnect(function() {
      return;
    });
  });
};

export const deleteUser = (id, dispatch) => {
  return axios
    .delete(`${process.env.REACT_APP_HOST}/user/delete/${id}`)
    .then(res => {
      dispatch(actions.gotUsers(res.data));
      return res.data;
    })
    .catch(err => {
      dispatch(actions.error(err));
      throw err;
    });
};

export const clearAlert = dispatch => {
  dispatch(actions.handledAlert());
};

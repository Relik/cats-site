export const GOT_USERS = "GOT_USERS";
export const ERROR = "ERROR";
export const HANDLED_ALERT = "HANDLED_ALERT";
export const RAISED_ALERT = "RAISED_ALERT";
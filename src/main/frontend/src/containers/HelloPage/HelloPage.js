import React from "react";
import Button from "@material-ui/core/Button";
import './hello-page.css';

export default props => {
  return (
    <div className="hello-page">
      <Button variant="contained" color="primary" onClick={()=>props.history.push('./register')}>
        Register
      </Button>
      <Button variant="contained" color="primary" onClick={()=>props.history.push('./users')}>
        Show Users
      </Button>
    </div>
  );
};

import React from "react";
import { Router, Route, Switch, Redirect } from "react-router-dom";
import "./root.css";
import Users from "../Users/Users";
import Register from "../Register/Register";
import { createBrowserHistory } from "history";
import HelloPage from "../HelloPage/HelloPage";
import NotFound from "../NotFound/NotFound";
import Header from "../../components/Header";
import { UserProvider } from "../../contexts/UserContext";
import AlertManager from "../../components/AlertManager/AlertManager";

export const history = createBrowserHistory();

export default () => {
  return (
    <div id="main">
      <Header history={history}/>
      <div className="d-flex flex-column justify-content-center align-items-center">
        <UserProvider>
          <Router history={history}>
            <Switch>
              <Route exact path="/users">
                <Users />
              </Route>
              <Route exact path="/register">
                <Register history={history} />
              </Route>
              <Route exact path="/404" component={NotFound} />
              <Route exact path="/" component={HelloPage} />
              <Redirect to="/404" />
            </Switch>
          </Router>
          <AlertManager/>
        </UserProvider>
      </div>
    </div>
  );
};

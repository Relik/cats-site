import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import { validateForm, formErrorsTemplate } from "./Validator";
import "./register.css";
import InputText from "../../components/InputText/InputText";
import * as validatorActions from "./Validator";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormLabel from "@material-ui/core/FormLabel";
import Checkbox from "@material-ui/core/Checkbox";
import { registerUser, userTemplate } from "../../actions/User/User";
import { useUserContext } from "../../contexts/UserContext";
import DropMenu from "../../components/DropMenu/DropMenu";
import { List, ListItem, Backdrop, CircularProgress } from "@material-ui/core";

const CatTypes = ["None", "Ameriacan Curl", "Abyssian", "Bengal Cat"];

export default props => {
  const { dispatch } = useUserContext();
  const [user, setUser] = useState(userTemplate);
  const [hideError, setHideError] = useState(true);
  const [formErrors, setFormErrors] = useState(formErrorsTemplate);
  const [isLoading, setIsLoading] = useState(false);

  const submitFrom = (e) => {
    e.preventDefault();
    if (isLoading) return;

    if (!validateForm(user)) {
      setHideError(false);
    } else {
      setIsLoading(true);
      registerUser(user, dispatch)
        .then(registredUser => {
          console.log(registredUser);
          setIsLoading(false);
          props.history.push("./");
        })
        .catch(err => {
          setIsLoading(false);
        });
    }
  };
  return (
    <div>
      <form>
        <List className="d-flex flex-column justify-content-center align-items-center">
          <ListItem>
            <InputText
              hideError={hideError}
              error={formErrors.forename}
              label="Forename"
              onChange={e => {
                setFormErrors({
                  ...formErrors,
                  forename: validatorActions.validateForename(e.target.value)
                });
                console.log(formErrors);
                setUser({ ...user, forename: e.target.value });
              }}
            />
          </ListItem>
          <ListItem>
            <InputText
              hideError={hideError}
              error={formErrors.surname}
              label="Surname"
              onChange={e => {
                setFormErrors({
                  ...formErrors,
                  surname: validatorActions.validateSurname(e.target.value)
                });
                setUser({ ...user, surname: e.target.value });
              }}
            />
          </ListItem>
          <ListItem divider>
            <InputText
              hideError={hideError}
              error={formErrors.email}
              label="Email"
              onChange={e => {
                setFormErrors({
                  ...formErrors,
                  email: validatorActions.validateEmail(e.target.value)
                });
                setUser({ ...user, email: e.target.value });
              }}
            />
          </ListItem>
          <ListItem>
            <div className="d-flex justify-content-between align-items-center">
              <FormLabel component="legend">Do you like cats?</FormLabel>
              <RadioGroup
                aria-label="cats like"
                className="justify-content-end align-items-center"
                name="likes"
                value={user.isCatLover ? "1" : "0"}
                onChange={e => {
                  setUser({ ...user, isCatLover: e.target.value === "1" });
                  setFormErrors({
                    ...formErrors,
                    dislike: validatorActions.validateDislike(user.reasonForDislike)
                  });
                }}
              >
                <FormControlLabel value="1" control={<Radio />} label="Yes" />
                <FormControlLabel value="0" control={<Radio />} label="No" />
              </RadioGroup>
            </div>
          </ListItem>
          <ListItem divider>
            {user.isCatLover ? (
              <DropMenu
                options={CatTypes}
                label={"Favouirite Breed"}
                value={user.favoriteBreed}
                onChange={e => {
                  setUser({ ...user, favoriteBreed: e.target.value });
                }}
              />
            ) : (
              <InputText
                hideError={hideError}
                error={formErrors.dislike}
                id="filled-basic"
                label="Why don’t you like cats?"
                variant="filled"
                value={user.reasonForDislike}
                onChange={e => {
                  setUser({ ...user, reasonForDislike: e.target.value });
                  setFormErrors({
                    ...formErrors,
                    dislike: validatorActions.validateDislike(e.target.value)
                  });
                }}
              />
            )}
          </ListItem>
          <ListItem>
            <FormControlLabel
              control={
                <Checkbox
                  checked={user.emailConfirmation}
                  onChange={e => {
                    setUser({
                      ...user,
                      emailConfirmation: e.target.checked
                    });
                  }}
                  name="email confirmation"
                  color="primary"
                />
              }
              label="Receive registration confirmation email"
            />
          </ListItem>
          <ListItem>
            <Button variant="contained" color="primary" type="submit" onClick={submitFrom} disabled={isLoading}>
              Register
            </Button>
            <Backdrop open={isLoading}>
              <CircularProgress color="inherit" />
            </Backdrop>
          </ListItem>
        </List>
      </form>
    </div>
  );
};


export const ErrorMessage = {
  NoError: "",
  Empty: "Cannot be empty",
  Length3: "Minimum 3 characters",
  Length10: "Minimum 10 characters",
  DrBegining: "Must start with 'Dr'",
  EmailFormat: "Incorrect email format",
  EmailTaken: "Email is already in use",
  Form: "Form is invalid"
};
export const formErrorsTemplate = {
  forename: ErrorMessage.Empty,
  surname: ErrorMessage.Empty,
  email: ErrorMessage.Empty,
  dislike: ""
};

export const validateForename = forename => {
  if (forename === "") {
    return ErrorMessage.Empty;
  } else if (forename.length < 3) {
    return ErrorMessage.Length3;
  } else {
    return ErrorMessage.NoError;
  }
};

export const validateSurname = surname => {
  if (surname === "") {
    return ErrorMessage.Empty;
  } else if (surname.substr(0, 2).toLowerCase() !== "dr") {
    return ErrorMessage.DrBegining;
  } else {
    return ErrorMessage.NoError;
  }
};

export const validateEmail = email => {
  if (email === "") {
    return ErrorMessage.Empty;
  } else if (
    !/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
      email
    )
  ) {
    return ErrorMessage.EmailFormat;
  } else {
    return ErrorMessage.NoError;
  }
};

export const validateDislike = dislike => {
  if (dislike !== "" && dislike.length < 10) {
    return ErrorMessage.Length10;
  } else {
    return ErrorMessage.NoError;
  }
};

export const validateForm = form => {
  if (
    validateForename(form.forename) ||
    validateSurname(form.surname) ||
    validateEmail(form.email) ||
    (!form.isCatLover && validateDislike(form.reasonForDislike))
  ) 
    return false;
  return true;
};


import React from "react";
import './not-found.css';
export default () => {
  return (
    <div>
      <img className="error404" src={"./error404.jpg"} alt="error 404 - page not found" />
    </div>
  );
};

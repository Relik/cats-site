import React, { useState, useEffect } from "react";
import { useUserContext } from "../../contexts/UserContext";
import { subscribeUsers, deleteUser, userTemplate, getUsers } from "../../actions/User/User";
import "./users.css";
import UsersTable from "../../components/UsersTable";
import ConfirmationDialog from "../../components/ConfirmationDialog/ConfirmationDialog";

export default () => {
  const { user, dispatch } = useUserContext();
  const [selectedUser, setUser] = useState(userTemplate);
  const [dialogOpen, setDialogOpen] = useState(false);

  useEffect(() => {
    let stompClient = undefined;
    getUsers(dispatch);
    async function sub(){
      stompClient = await subscribeUsers(dispatch);
    }
    sub();
    return () => {
      stompClient && stompClient.disconnect()
    };
  }, [dispatch]);

  const handleCloseDialog = (conf = false) => {
    if (conf) {
      deleteUser(selectedUser.id, dispatch);
    }
    setDialogOpen(false);
  };
  const handleRowDoubleClick = user => {
    setUser(user);
    setDialogOpen(true);
  };
  return (
    <div className="users-table">
      <UsersTable 
        users={user.users}
        onRowDoubleClick={handleRowDoubleClick}
      ></UsersTable>
      <ConfirmationDialog
        open={dialogOpen}
        onClose={handleCloseDialog}
        message={`Do you really want to delete user: ${selectedUser.forename} ${selectedUser.surname}?`}
      ></ConfirmationDialog>
    </div>
  );
};

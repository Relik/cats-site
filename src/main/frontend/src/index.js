import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Root from './containers/Root/Root';
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';

ReactDOM.render(
  <React.StrictMode>
    <Root />
  </React.StrictMode>,
  document.getElementById('root')
);

serviceWorker.register();

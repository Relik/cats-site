package com.relik.cats.Exceptions;

public class IncorrectSurnameException extends Exception {
    public IncorrectSurnameException(String errorMessage) {
        super(errorMessage);
    }
}

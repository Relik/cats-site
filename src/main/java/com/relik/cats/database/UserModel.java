package com.relik.cats.database;

import com.relik.cats.modelDTO.UserDTO;

import javax.persistence.*;

@Entity
public class UserModel {
    private @Id @GeneratedValue Long id;
    private String forename;
    private String surname;
    @Column(unique = true)
    private  String email;
    private boolean isCatLover;
    private String favoriteBreed; //@IDEA: combine fav and reason into one variable?
    private String reasonForDislike;
    private boolean emailConfirmation;

    public UserModel(){
        super();
    }
    public UserModel(String forename, String surname, String email, boolean isCatLover, String favoriteBreed, String reasonForDislike, boolean emailConfirmation) {
        this.forename = forename;
        this.surname = surname;
        this.email = email;
        this.isCatLover = isCatLover;
        this.favoriteBreed = favoriteBreed;
        this.reasonForDislike = reasonForDislike;
        this.emailConfirmation = emailConfirmation;
    }
    public UserModel(UserDTO user) {
        this.forename = user.getForename();
        this.surname = user.getSurname();
        this.email = user.getEmail();
        this.isCatLover = user.getIsCatLover();
        this.favoriteBreed = user.getFavoriteBreed();
        this.reasonForDislike = user.getReasonForDislike();
        this.emailConfirmation = user.getEmailConfirmation();
    }

    public void setForename(String forename) {
        this.forename = forename;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setIsCatLover(boolean isCatLover) {
        this.isCatLover = isCatLover;
    }

    public void setFavoriteBreed(String favoriteBreed) {
        this.favoriteBreed = favoriteBreed;
    }

    public void setReasonForDislike(String reasonForDislike) {
        this.reasonForDislike = reasonForDislike;
    }

    public void setEmailConfirmation(boolean emailConfirmation) {
        this.emailConfirmation = emailConfirmation;
    }

    public Long getId() {
        return id;
    }

    public String getForename() {
        return forename;
    }

    public String getSurname() {
        return surname;
    }

    public String getEmail() {
        return email;
    }

    public boolean getIsCatLover() {
        return isCatLover;
    }

    public String getFavoriteBreed() {
        return favoriteBreed;
    }

    public String getReasonForDislike() {
        return reasonForDislike;
    }

    public boolean getEmailConfirmation() {
        return emailConfirmation;
    }
}

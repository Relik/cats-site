package com.relik.cats.database;

import com.relik.cats.database.UserModel;
import org.apache.catalina.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<UserModel, Long> {
}

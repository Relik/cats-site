package com.relik.cats.controllers;

import com.relik.cats.Exceptions.IncorrectSurnameException;
import com.relik.cats.modelDTO.UserDTO;
import com.relik.cats.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;

@Controller
@RequestMapping("/user")
public class UserController {
    private final static Logger LOG = Logger.getLogger(UserController.class.getName());

    private UserService userService;
    @Autowired
    private SimpMessagingTemplate webSocket;
    @Autowired
    UserController(UserService userService){
        this.userService = userService;
    }

    @PostMapping("/register")
    public ResponseEntity<Object> registerUser(@RequestBody UserDTO user){
        try {
            user.validateSurname();
            userService.registerUser(user);
            if(user.getEmailConfirmation())LOG.info("Sending email confirmation to " + user.getEmail());
            List<UserDTO> users = userService.getUsers();
            webSocket.convertAndSend("/new", users);
            return ResponseEntity.ok(users);
        } catch (IncorrectSurnameException e) {
            LOG.warning(e.getMessage());
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(e.getMessage());
        } catch (Exception e) {
            LOG.warning(e.getMessage());
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("Email is taken");
        }
    }

    @GetMapping("/users")
    public ResponseEntity<List<UserDTO>> getUsers() throws Exception{
        List<UserDTO> users = userService.getUsers();
        webSocket.convertAndSend("/new", users);
        return ResponseEntity.ok(users);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<List<UserDTO>> delete(@PathVariable int id) throws Exception {
        LOG.info("Deleting User: " + id);
        userService.deleteUser(id);
        List<UserDTO> users = userService.getUsers();
        webSocket.convertAndSend("/new", users);
        return ResponseEntity.ok(users);
    }
}

package com.relik.cats.controllers;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;


@Controller
public class RootController {
    private final static Logger LOG = Logger.getLogger(ErrorController.class.getName());

    @RequestMapping("/")
    public String index() {
        return "index.html";
    }


}

package com.relik.cats.services;

import com.relik.cats.modelDTO.UserDTO;

import java.util.List;

public interface UserService {
    List<UserDTO> getUsers();
    void registerUser(UserDTO user);
    void deleteUser(long id);
}

package com.relik.cats.services;

import com.relik.cats.database.UserModel;
import com.relik.cats.database.UserRepository;
import com.relik.cats.modelDTO.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserRepository userRepository;

    @Override
    public void deleteUser(long id) {
        userRepository.deleteById(id);
    }

    @Override
    public List<UserDTO> getUsers() {
        Iterable<UserModel> users = userRepository.findAll();

        List<UserDTO> usersDto = new ArrayList<>();
        for (UserModel user:
             users) {
            usersDto.add(new UserDTO(user));
        }
        return usersDto;
    }

    @Override
    public void registerUser(UserDTO user) {
        UserModel newUser = new UserModel(user);
        userRepository.save(newUser);
    }
}

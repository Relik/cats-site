package com.relik.cats.modelDTO;

import com.relik.cats.Exceptions.IncorrectSurnameException;
import com.relik.cats.database.UserModel;

import java.util.logging.Logger;


public class UserDTO {
    private final static Logger LOG = Logger.getLogger(UserDTO.class.getName());

    private Long id;
    private String forename;
    private String surname;
    private String email;
    private boolean isCatLover;
    private String favoriteBreed; //@IDEA: combine fav and reason into one variable?
    private String reasonForDislike;
    private boolean emailConfirmation;

    public UserDTO()
    {
        super();
    }
    public UserDTO( String forename, String surname, String email, boolean isCatLover, String favoriteBreed, String reasonForDislike, boolean emailConfirmation)  {
        this.forename = forename;
        this.surname = surname;
        this.email = email;
        this.isCatLover = isCatLover;
        this.favoriteBreed = favoriteBreed;
        this.reasonForDislike = reasonForDislike;
        this.emailConfirmation = emailConfirmation;
    }
    public UserDTO(UserModel user) {
        this.id = user.getId();
        this.forename = user.getForename();
        this.surname = user.getSurname();
        this.email = user.getEmail();
        this.isCatLover = user.getIsCatLover();
        this.favoriteBreed = user.getFavoriteBreed();
        this.reasonForDislike = user.getReasonForDislike();
        this.emailConfirmation = user.getEmailConfirmation();
    }
    public boolean validateSurname() throws IncorrectSurnameException {
        if(this.surname.length() >= 4 && this.surname.substring(0,4).toLowerCase().equals("drop"))
            throw new IncorrectSurnameException("Surname cannot begin with 'drop'");
        return true;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public void setForename(String forename) {
        this.forename = forename;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setIsCatLover(boolean isCatLover) {
        this.isCatLover = isCatLover;
    }

    public void setFavoriteBreed(String favoriteBreed) {
        this.favoriteBreed = favoriteBreed;
    }

    public void setReasonForDislike(String reasonForDislike) {
        this.reasonForDislike = reasonForDislike;
    }

    public void setEmailConfirmation(boolean emailConfirmation) {
        this.emailConfirmation = emailConfirmation;
    }

    public Long getId() {
        return id;
    }

    public String getForename() {
        return forename;
    }

    public String getSurname() {
        return surname;
    }

    public String getEmail() {
        return email;
    }

    public boolean getIsCatLover() {
        return isCatLover;
    }

    public String getFavoriteBreed() {
        return favoriteBreed;
    }

    public String getReasonForDislike() {
        return reasonForDislike;
    }

    public boolean getEmailConfirmation() {
        return emailConfirmation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserDTO userDTO = (UserDTO) o;

        if (isCatLover != userDTO.isCatLover) return false;
        if (emailConfirmation != userDTO.emailConfirmation) return false;
        if (!id.equals(userDTO.id)) return false;
        if (!forename.equals(userDTO.forename)) return false;
        if (!surname.equals(userDTO.surname)) return false;
        if (!email.equals(userDTO.email)) return false;
        if (favoriteBreed != null ? !favoriteBreed.equals(userDTO.favoriteBreed) : userDTO.favoriteBreed != null)
            return false;
        return reasonForDislike != null ? reasonForDislike.equals(userDTO.reasonForDislike) : userDTO.reasonForDislike == null;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + forename.hashCode();
        result = 31 * result + surname.hashCode();
        result = 31 * result + email.hashCode();
        result = 31 * result + (isCatLover ? 1 : 0);
        result = 31 * result + (favoriteBreed != null ? favoriteBreed.hashCode() : 0);
        result = 31 * result + (reasonForDislike != null ? reasonForDislike.hashCode() : 0);
        result = 31 * result + (emailConfirmation ? 1 : 0);
        return result;
    }
}

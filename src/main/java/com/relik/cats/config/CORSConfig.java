package com.relik.cats.config;

import com.relik.cats.controllers.UserController;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.logging.Logger;

@Configuration
public class CORSConfig implements WebMvcConfigurer {
    private final static Logger LOG = Logger.getLogger(UserController.class.getName());

    @Value("${FRONT}")
    private String frontUrl;
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        LOG.info(frontUrl);
        registry.addMapping("/**").allowedOrigins(frontUrl).allowedMethods("*").allowedHeaders("*").allowCredentials(true);

    }
}